define(function(require){
  // Process New Image File
  // static counter
  var imageCount = 0;
  // 
  function processImageFile(file){
    if (! file.type.match(/image.*/))
      return;
    // DOM
    var localNode = document.createElement('div');
    var imageAreaNode = document.createElement('div');
    document.body.appendChild(localNode);
    localNode.appendChild(imageAreaNode);
    // New Image
    var image = new Image();
    image.onload = function(e){
                     imageAreaNode.appendChild(e.target);
                     // Invoke benchmarking
                     benchmark(e.target, localNode);
                   }
    image.file = file;
    image.id = "image-" + imageCount++;
    // Process Loading
    var reader = new FileReader();
    reader.onload = function(e){ image.src = e.target.result; }
    reader.readAsDataURL(file);
  }
  
  // Set Drag & Drop Event
  document.addEventListener("dragenter", function(e){
    e.stopPropagation();
    e.preventDefault();
  });
  document.addEventListener("dragover", function(e){
    e.stopPropagation();
    e.preventDefault();
  });
  document.addEventListener("drop", function(e){
    e.stopPropagation();
    e.preventDefault();
    var files = e.dataTransfer.files;
    for (var i = 0; i < files.length; ++i) {
      processImageFile(files.item(i));
    }
  });
  
  // Benchmark code
  function benchmark(image, localNode)
  {
    // Each Filter
    [ 'invert'
    ].forEach(function(filter_name){
      var node = document.createElement('div');
      localNode.appendChild(node);
      // Load Filter
      require(['./filters/'+filter_name], function(Filter){
        // New Filter
        var filter = new Filter();
        // Create Canvas
        var canvas = document.createElement('canvas');
        node.appendChild(canvas);
        // Setup
        filter.oncomplete = function(profilingResult){
                              var node1 = buildOutputTable(profilingResult);
                              console.log(profilingResult);
                              node.appendChild(node1);
                            }
        filter.source = image;
        filter.destination = canvas;
        // Run
        filter.run();
        // Cleanup
        filter.dispose();
      });
    });
  }
  
  function buildOutputTable(profilingResult)
  {
    var tnode = document.createElement('table');
    for (var i in profilingResult) {
      var tr = document.createElement('tr');
      var td1 = document.createElement('td');
      var td2 = document.createElement('td');
      tnode.appendChild(tr);
      tr.appendChild(td1);
      tr.appendChild(td2);
      td1.textContent = i;
      td2.textContent = profilingResult[i] / 1000 + " us";
    }
    return tnode;
  }
  
});
