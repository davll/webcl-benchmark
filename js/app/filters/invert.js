define([
  './basic', 'text!./invert.cl'
], function(BasicFilter, code)
{
  
  // InvertFilter class (inherits BasicFilter)
  function InvertFilter()
  {
    BasicFilter.call(this, code);
    this._kernel = this._program.createKernel('invert');
  }
  
  // Inherit BasicFilter
  InvertFilter.prototype = Object.create(BasicFilter.prototype);
  InvertFilter.prototype.constructor = InvertFilter;
  
  // run method
  InvertFilter.prototype.run = function()
  {
    // abbrevation
    var ctx = this._context, cmd = this._commandQueue;
    // create images
    var data1 = this.getSourceData();
    var width = data1.width, height = data1.height;
    var format = {
      channelOrder: WebCL.CL_RGBA,
      channelDataType: WebCL.CL_UNORM_INT8
    }
    var img1 = ctx.createImage2D(WebCL.CL_MEM_READ_ONLY, format, width, height, 0);
    var img2 = ctx.createImage2D(WebCL.CL_MEM_WRITE_ONLY, format, width, height, 0);
    // load source image
    var evt1 = cmd.enqueueWriteImage(img1, false, [0,0,0], [width, height, 1], 0, 0, data1.data, []);
    // set kernel params
    this._kernel.setKernelArg(0, img1);
    this._kernel.setKernelArg(1, img2);
    // compute
    var lsz = 16;
    var kwid = Math.ceil(width / lsz) * lsz, khei = Math.ceil(height / lsz) * lsz;
    var evt2 = cmd.enqueueNDRangeKernel(this._kernel, 2, [0,0], [kwid, khei], [lsz, lsz], []);
    // writeback destination image
    var data2 = this.getDestinationData(img2);
    var evt3 = cmd.enqueueReadImage(img2, false, [0,0,0], [width, height, 1], 0, 0, data2.data, []);
    // wait for all
    cmd.finish();
    // compute time
    WebCL.waitForEvents([evt1, evt2, evt3]);
    var d1_g = evt1.getEventProfilingInfo(WebCL.CL_PROFILING_COMMAND_END) - 
               evt1.getEventProfilingInfo(WebCL.CL_PROFILING_COMMAND_START),
        d2_g = evt2.getEventProfilingInfo(WebCL.CL_PROFILING_COMMAND_END) - 
               evt2.getEventProfilingInfo(WebCL.CL_PROFILING_COMMAND_START),
        d3_g = evt3.getEventProfilingInfo(WebCL.CL_PROFILING_COMMAND_END) - 
               evt3.getEventProfilingInfo(WebCL.CL_PROFILING_COMMAND_START);
    var result = {
      LoadTime: d1_g,
      ComputationTime: d2_g,
      WriteBackTime: d3_g
    }
    this.oncomplete(result);
    // display result
    this.putDestinationData(data2);
  }
  
  // export
  return InvertFilter;
});
