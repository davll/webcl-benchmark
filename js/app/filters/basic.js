define(function(){
  
  // BasicFilter class
  function BasicFilter(programCode)
  {
    // Create OpenCL Context
    var context = (function(platform){
                    var props = [WebCL.CL_CONTEXT_PLATFORM, platform];
                    var type = WebCL.CL_DEVICE_TYPE_GPU;
                    return WebCL.createContextFromType(props, type);
                  }) (WebCL.getPlatformIDs()[0]);
    //
    this._context = context;
    this._device = context.getContextInfo(WebCL.CL_CONTEXT_DEVICES)[0];
    this._commandQueue = context.createCommandQueue(this._device, WebCL.CL_QUEUE_PROFILING_ENABLE);
    // compile program
    this._program = context.createProgramWithSource(programCode);
    try {
      this._program.buildProgram([this._device], "");
    } catch(e) {
      var dev = this._device, prog = this._program;
      var status = prog.getProgramBuildInfo(dev,
                                            WebCL.CL_PROGRAM_BUILD_STATUS);
      console.log("Failed to build WebCL program. Error " + status + ": " +
                  prog.getProgramBuildInfo(dev, WebCL.CL_PROGRAM_BUILD_LOG)
                  );
      throw e;
    }
  }
  
  // getSourceData method
  BasicFilter.prototype.getSourceData = function()
  {
    var src = this.source;
    var width = src.width, height = src.height;
    var canvas = document.createElement('canvas');
    canvas.width = width, canvas.height = height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(src, 0, 0, width, height);
    return ctx.getImageData(0, 0, width, height);
  }
  
  // getDestinationData method
  BasicFilter.prototype.getDestinationData = function(image)
  {
    var cmd = this._commandQueue;
    var width = image.getImageInfo(WebCL.CL_IMAGE_WIDTH),
        height = image.getImageInfo(WebCL.CL_IMAGE_HEIGHT);
    var canvas = this.destination;
    canvas.width = width, canvas.height = height;
    var cxt = canvas.getContext("2d");
    return cxt.createImageData(width, height);
  }
  
  // putDestinationData
  BasicFilter.prototype.putDestinationData = function(data)
  {
    var canvas = this.destination;
    var ctx = canvas.getContext("2d");
    ctx.putImageData(data, 0, 0);
  }
  
  // dispose method
  BasicFilter.prototype.dispose = function()
  {
    this._program.releaseCLResources();
    this._commandQueue.releaseCLResources();
    this._context.releaseCLResources();
  }
  
  // export
  return BasicFilter;
});
