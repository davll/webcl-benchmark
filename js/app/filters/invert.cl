const sampler_t sampler = CLK_NORMALIZED_COORDS_TRUE |
                          CLK_ADDRESS_CLAMP_TO_EDGE |
                          CLK_FILTER_NEAREST;

__kernel void invert(__read_only image2d_t src,
                    __write_only image2d_t dst)
{
  int x = get_global_id(0), y = get_global_id(1);
  int2 dim = get_image_dim(dst);
  if (x >= dim.x || y >= dim.y)
    return;
  float tx = x / (float)dim.x, ty = y / (float)dim.y;
  float4 color = read_imagef(src, sampler, (float2)(tx, ty));
  float4 result = (float4)((float3)(1.0f) - color.xyz, 1.0f);
  write_imagef(dst, (int2)(x, y), result);
}
